------------------------------------------------------
If you are running jdk 1.7.0_71 in the default location
------------------------------------------------------
1. Run the file "compile.bat"

2. Open the command prompt (CTRL+R and type "cmd")

3. Navigate to the folder containing BT.jar

4 run the command "java -jar bt.jar input.txt foo bar".
In the above line:
Replace input.txt with the path to the input file.
Replace "foo" and "bar" with the two names that you would like to find a path between.


To compile the source code run "compile.bat". This will compile the source code with the assumption that the computer is running jdk1.7.0.71.

java -jar BT.jar "C:\\Users\\TheGeneral\\OneDrive\\Documents\\Jobs\\Applied\\BT\\atchm_3803 2.txt" "Black Widow" "Hit Girl"