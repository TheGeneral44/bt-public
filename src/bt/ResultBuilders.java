/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TheGeneral
 */
public class ResultBuilders {
    EmployeeList list;
    Result result;
    ArrayList<String> finalStrings;
    
    public ResultBuilders(EmployeeList l) {
        list = l;
        result = new Result();
        finalStrings = new ArrayList();
    }
    public void buildList() {
        while (!list.isEmployeeAEmpty()) { //Form list of employees matching first name
            LinkedList ll = new LinkedList();
            String emp = list.popNextEmployeeA();
            ll.add(emp);
            result.addLinksA(ll);
        }
        while (!list.isEmployeeBEmpty()) { //Form list of employees matching second name
            LinkedList ll = new LinkedList();
            String emp = list.popNextEmployeeB();
            ll.add(emp);
            result.addLinksB(ll);
        }
        boolean hasfinished = false;
        int i = 0;
        while (!hasfinished) {
            //Index value; determines which LinkedList to analyse
            i = i % (result.linksASize() + result.linksBSize());
            LinkedList empls;
            if (i < result.linksASize()) {
                empls = result.getLinksA(i);
            } else {
                empls = result.getLinksB(i - result.linksASize());
            }
            //
            //error check to see if top of tree needed here
            if (!list.get((String) empls.getLast()).getManager().isEmpty()) {
                empls.add(list.get((String) empls.getLast()).getManager()); //add the top's manager to the top
            }
            i++;
            //check if the linkedlists have intersected
            for (LinkedList tempList : result.getLinksA()) {
                for (LinkedList tempList2 : result.getLinksB()) {
                    if (tempList.getLast().equals(tempList2.getLast())) {
                        hasfinished = true;
                    }
                }
            }
        }
    }
    public void buildStrings() {
        for (LinkedList<String> tempList : result.getLinksA()) {
            StringBuilder sb = new StringBuilder();
            for (String s : tempList) {
                sb.append(list.get(s).getName()).append("(")
                        .append(list.get(s).getID()).append(")");
                if (!s.equalsIgnoreCase(tempList.getLast())) {
                    sb.append(" -> ");
                }
            }
            for (LinkedList<String> tempList2 : result.getLinksB()) {
                for (Iterator<String> it = tempList2.descendingIterator(); it.hasNext();) {
                    String s = it.next();
                    if (tempList.getLast().equalsIgnoreCase(s)) {
                    
                    } else {
                        sb.append(" <- ").append(list.get(s).getName())
                                .append("(").append(list.get(s).getID())
                                .append(")");
                    }
                }
                finalStrings.add(sb.toString());
            }
        } 
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String s : finalStrings) {
            sb.append(s);
                    if (!s.equals(finalStrings.get(finalStrings.size()-1))) {
                        sb.append("\n");
                    }
        }
        return sb.toString();
    }
    
    public class BuildList implements Runnable {

        LinkedList empls;
        int threadNum;

        BuildList(LinkedList e, int i) {
            empls = e;
            threadNum = i;
        }

        @Override
        public void run() {
            boolean hasfinished = false;
            while (!hasfinished) {
                if (!list.get((String) empls.getLast()).getManager().isEmpty()) {
                    empls.add(list.get((String) empls.getLast()).getManager()); //add the top's manager to the top
                }
                if (Thread.interrupted()) {
                    // We've been interrupted: no more crunching.
                    hasfinished=true;
                }
            }
        }
    }
    
    public class ListChecker implements Runnable {
        ArrayList<Thread> threads;

        ListChecker(ArrayList<Thread> t) {
            threads = (ArrayList<Thread>) t.clone();
        }

        @Override
        public void run() {
            boolean hasfinished = false;
            while (!hasfinished) {
                for (LinkedList tempList : result.getSafeLinksA()) {
                    for (LinkedList tempList2 : result.getSafeLinksB()) {
                        if (tempList.getLast().equals(tempList2.getLast())) {
                            for (Thread t : threads) {
                                t.interrupt();
                            }
                            hasfinished = true;
                        }
                    }
                }
            }
        }
        
    }

    public void buildListThreaded() {
    while (!list.isEmployeeAEmpty()) { //Form list of employees matching first name
            LinkedList ll = new LinkedList();
            String emp = list.popNextEmployeeA();
            ll.add(emp);
            result.addLinksA(ll);
        }
        while (!list.isEmployeeBEmpty()) { //Form list of employees matching second name
            LinkedList ll = new LinkedList();
            String emp = list.popNextEmployeeB();
            ll.add(emp);
            result.addLinksB(ll);
        }
        int i = 0;
        ArrayList<Thread> threads = new ArrayList();
        Thread t;
        t = new Thread(new ListChecker(threads));
        t.start();
        threads.add(t);
        while (i<(result.linksASize()+result.linksBSize())) {
            //Index value; determines which LinkedList to analyse
            i = i % (result.linksASize() + result.linksBSize());
            LinkedList empls;
            if (i < result.linksASize()) {
                empls = result.getLinksA(i);
                t = new Thread(new BuildList(empls, i));
                t.start();
                threads.add(t);
            } else {
                empls = result.getLinksB(i - result.linksASize());
                t = new Thread(new BuildList(empls,i - result.linksASize()));
                t.start();
                threads.add(t);
            }
            i++;
        }
        for (Thread thread : threads) {
        try {
            thread.join();
        } catch (InterruptedException ex) {
            Logger.getLogger(ResultBuilders.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    }
}
