/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bt;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author David Stephens
 */
public class ImportStaff {
    public static EmployeeList readFile(String fileName, String first, String last) {
    EmployeeList list = new EmployeeList();
    //searching a hashmap without the key is slow
    //so we're going to return the requested employee IDs
    String firstEmployee = first;
    String lastEmployee = last;
    try {
            FileReader fr;
            fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            line = br.readLine();
            line = trimSpaces(line);
            String[] split;
            while (line != null) {
            if (!line.equals("")) {
                split = line.split("\\|");
                while (split[1].length() == 0) { //if string is blank, read next line
                    line = br.readLine();
                    line = trimSpaces(line);
                    split = line.split("\\|");
                }
                if (split.length == 3) {
                    Employee tempEmployee = new Employee(split[1], split[2]);
                    list.put(split[1], tempEmployee);
                    if (split[2].equalsIgnoreCase(firstEmployee)) {
                        list.addEmployeeA(split[1]);
                    }
                    if (split[2].equalsIgnoreCase(lastEmployee)) {
                        list.addEmployeeB(split[1]);
                    }
                } else if (split.length >= 4) {
                    Employee tempEmployee = new Employee(split[1], split[2], split[3]);
                    list.put(split[1], tempEmployee);
                    if (split[2].equalsIgnoreCase(firstEmployee)) {
                        list.addEmployeeA(split[1]);
                    }
                    if (split[2].equalsIgnoreCase(lastEmployee)) {
                        list.addEmployeeB(split[1]);
                    }
                } else {
                    Logger.getLogger(ImportStaff.class.getName()).log(Level.WARNING, "Error reading line");
                }
            }
            line = br.readLine();
            //check that the line we're stripping isn't null
            if (line != null) {
                line = trimSpaces(line);
            }

        }
            br.close();
        } catch (IOException ex) {
            Logger.getLogger(ImportStaff.class.getName()).log(Level.SEVERE, null, ex);
        }
    return list;
    }
    
    private static String trimSpaces(String s) {
        while(s.contains("  ")) {
            s = s.replace("  ", " ");
        }
        s = s.replace(" |", "|");
        s = s.replace("| ", "|");
        return s;
    }
}
