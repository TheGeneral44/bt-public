/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bt;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author TheGeneral
 */
public class Result {
    private ArrayList<LinkedList> linksA;
    private ArrayList<LinkedList> linksB;

    public Result() {
        linksA = new ArrayList<>();
        linksB = new ArrayList<>();
    }
    
    /**
     * @return the linksA
     */
    public ArrayList<LinkedList> getLinksA() {
        return linksA;
    }

    /**
     * @return the linksB
     */
    public ArrayList<LinkedList> getLinksB() {
        return linksB;
    }
    
        /**
     * @return the linksA
     */
    public ArrayList<LinkedList> getSafeLinksA() {
        return (ArrayList<LinkedList>) linksA.clone();
    }

    /**
     * @return the linksB
     */
    public ArrayList<LinkedList> getSafeLinksB() {
        return (ArrayList<LinkedList>) linksB.clone();
    }
    /**
     * @param i index of the LinkedList to retrieve
     * @return the linksA
     */
    public LinkedList getLinksA(int i) {
        return getLinksA().get(i);
    }

    /**
     * @param i index of the LinkedList to retrieve
     * @return the linksB
     */
    public LinkedList getLinksB(int i) {
        return getLinksB().get(i);
    }
    
    /**
     * @param i index of the LinkedList to retrieve
     * @return the linksA
     */
    public LinkedList getSafeLinksA(int i) {
        return (LinkedList) getLinksA().get(i).clone();
    }

    /**
     * @param i index of the LinkedList to retrieve
     * @return the linksB
     */
    public LinkedList getSafeLinksB(int i) {
        return (LinkedList) getLinksB().get(i).clone();
    }

    public void addLinksA(LinkedList list) {
        linksA.add(list);
    }
    
    public void addLinksB(LinkedList list) {
        linksB.add(list);
    }
   
    public int linksASize() {
        return getLinksA().size();
    }
    public int linksBSize() {
        return getLinksB().size();
    }
    
}
