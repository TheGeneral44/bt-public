/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bt;

import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author David Stephens
 * 
 * Class for storing the hashmap of employees alongside the names of the
 * employees we are searching for. Methods mirror those of a hashmap
 */
public class EmployeeList {
    HashMap<String,Employee> employeeList;
    LinkedList<String> employeeA;
    LinkedList<String> employeeB;
    public EmployeeList(){
        employeeList = new HashMap<>();
        employeeA = new LinkedList();
        employeeB = new LinkedList();
    }
    public EmployeeList(String first, String last) {
        employeeList = new HashMap<>();
        employeeA = new LinkedList();
        employeeB = new LinkedList();
    }
    public void put(String ID, Employee employee) {
        employeeList.put(ID, employee);
    }
    public Employee get (String ID) {
        return employeeList.get(ID);
    }
    public void addEmployeeA(String ID) {
        employeeA.add(ID);
    }
    public void addEmployeeB(String ID) {
        employeeB.add(ID);
    }
    public String popNextEmployeeA() {
        return employeeA.pop();
    }
    public String popNextEmployeeB() {
        return employeeB.pop();
    }
    public boolean isEmployeeAEmpty() {
        return employeeA.isEmpty();
    }
    public boolean isEmployeeBEmpty() {
        return employeeB.isEmpty();
    }
    public int[] count() {
        int[] c = new int[2];
        c[0] = employeeA.size();
        c[1] = employeeB.size();
        return c;
    }
}
