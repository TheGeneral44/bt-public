/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author David Stephens
 */
public class BT {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
      
        EmployeeList list = ImportStaff.readFile(args[0],args[1],args[2]);
        /*
        for (Employee e = list.get(emp); e != null; e= list.get(e.getManager())){
                ll.add(e.getID());
        }*/
        ResultBuilders result = new ResultBuilders(list);
        result.buildList();
        result.buildStrings();
        System.out.println(result);
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTimeSingle = stopTime - startTime;
        System.out.println("Single thread:" +elapsedTimeSingle);
        
        startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
      
        EmployeeList list = ImportStaff.readFile(args[0],args[1],args[2]);
        /*
        for (Employee e = list.get(emp); e != null; e= list.get(e.getManager())){
                ll.add(e.getID());
        }*/
        ResultBuilders result = new ResultBuilders(list);
        result.buildListThreaded();
        result.buildStrings();
        System.out.println(result);
        }
        stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Single thread:" +elapsedTimeSingle);
        System.out.println("Multi threaded:" +elapsedTime);
    }
    
}
