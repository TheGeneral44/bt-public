/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bt;

/**
 *
 * @author TheGeneral
 */
public class Employee {
    //Not actually using the IDs as numbers; not perforing any arithmetic work
    private String ID;
    private String name;
    private String manager;
    public Employee(String ID, String name, String manager){
        this.ID = ID;
        this.name=name;
        this.manager=manager;
    }
    public Employee(String ID, String name) {
        this(ID, name,"");
    }

    /**
     * @return the ID
     */
    public String getID() {
        return ID;
    }

    /**
     * @param ID the ID to set
     */
    public void setID(String ID) {
        this.ID = ID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the manager
     */
    public String getManager() {
        return manager;
    }

    /**
     * @param manager the manager to set
     */
    public void setManager(String manager) {
        this.manager = manager;
    }
}
