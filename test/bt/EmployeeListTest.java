/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bt;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author TheGeneral
 */
public class EmployeeListTest {
    
    public EmployeeListTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of put method, of class EmployeeList.
     */
    @Test
    public void testPut() {
        System.out.println("put");
        String ID = "1";
        Employee employee = new Employee(ID, "Test");
        EmployeeList instance = new EmployeeList();
        instance.put(ID, employee);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(ID, instance.get(ID).getID());
        assertEquals(employee.getName(), instance.get(ID).getName());
    }

    /**
     * Test of get method, of class EmployeeList.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        String ID = "";
        EmployeeList instance = new EmployeeList();
        Employee expResult = null;
        Employee result = instance.get(ID);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addEmployeeA method, of class EmployeeList.
     */
    @Test
    public void testAddEmployeeA() {
        System.out.println("addEmployeeA");
        String ID = "";
        EmployeeList instance = new EmployeeList();
        instance.addEmployeeA(ID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addEmployeeB method, of class EmployeeList.
     */
    @Test
    public void testAddEmployeeB() {
        System.out.println("addEmployeeB");
        String ID = "";
        EmployeeList instance = new EmployeeList();
        instance.addEmployeeB(ID);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of popNextEmployeeA method, of class EmployeeList.
     */
    @Test
    public void testPopNextEmployeeA() {
        System.out.println("popNextEmployeeA");
        EmployeeList instance = new EmployeeList();
        String expResult = "";
        String result = instance.popNextEmployeeA();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of popNextEmployeeB method, of class EmployeeList.
     */
    @Test
    public void testPopNextEmployeeB() {
        System.out.println("popNextEmployeeB");
        EmployeeList instance = new EmployeeList();
        String expResult = "";
        String result = instance.popNextEmployeeB();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isEmployeeAEmpty method, of class EmployeeList.
     */
    @Test
    public void testIsEmployeeAEmpty() {
        System.out.println("isEmployeeAEmpty");
        EmployeeList instance = new EmployeeList();
        boolean expResult = false;
        boolean result = instance.isEmployeeAEmpty();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of isEmployeeBEmpty method, of class EmployeeList.
     */
    @Test
    public void testIsEmployeeBEmpty() {
        System.out.println("isEmployeeBEmpty");
        EmployeeList instance = new EmployeeList();
        boolean expResult = false;
        boolean result = instance.isEmployeeBEmpty();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of count method, of class EmployeeList.
     */
    @Test
    public void testCount() {
        System.out.println("count");
        EmployeeList instance = new EmployeeList();
        int[] expResult = null;
        int[] result = instance.count();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
